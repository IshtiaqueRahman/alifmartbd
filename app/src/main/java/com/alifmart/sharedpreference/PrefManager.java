package com.alifmart.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    Context context;

    public PrefManager(Context context) {
        this.context = context;
    }

    public void saveUserToken(String userToken) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserToken", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token", userToken);
        editor.commit();
    }

    public String getUserToken() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserToken", Context.MODE_PRIVATE);
        return sharedPreferences.getString("token", "");
    }

    public boolean isUserLogedOut() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserToken", Context.MODE_PRIVATE);
        boolean isTokenEmpty = sharedPreferences.getString("token", "").isEmpty();
        return isTokenEmpty;
    }
}
