package com.alifmart.apiutils;

import com.alifmart.models.ProductsPojo;
import com.alifmart.models.SliderImagesPojo;
import com.alifmart.models.brandproducts.BrandProductsPojo;
import com.alifmart.models.categoryproducts.CategoryProductsPojo;
import com.alifmart.models.listcategory.CategorySubCategoryPojo;
import com.alifmart.models.productdetails.ProductDetailsPojo;
import com.alifmart.models.loginregistration.Login;
import com.alifmart.models.loginregistration.Registration;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("slider-images")
    Call<SliderImagesPojo> getSliderImages();

    @GET("vendor-product")
    Call<ProductsPojo> getAllProducts();

    @POST("brands")
    Call<BrandProductsPojo> getAllProductsForBrand(@Query("brand_id") int brandId);

    @POST("categories")
    Call<CategoryProductsPojo> getAllProductsForCategory(@Query("category_id") int categoryId);

    @POST("category-list")
    Call<CategorySubCategoryPojo> getAllCategorySubCategory();

    @POST("product-details")
    Call<ProductDetailsPojo> getProductDetails(@Query("product_id") int productId);

    @POST("login")
    Call<Login> getLoginStatus(@Query("email") String email, @Query("password") String password);

    @POST("register")
    Call<Registration> getUserRegistrationStatus(@Query("name") String name, @Query("email") String email, @Query("phone") String phone, @Query("password") String passwprd);
}
