package com.alifmart.cache.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "cart_product_details")
public class CartProductDetails {

    @PrimaryKey
    @ColumnInfo(name = "product_id")
    int productId;

    @ColumnInfo(name = "product_name")
    String productName;

    @ColumnInfo(name = "product_price")
    double productPrice;

    @ColumnInfo(name = "product_thumbnail")
    String productThumbnail;

    @ColumnInfo(name = "product_quantity")
    int productQuantity;

    public CartProductDetails(int productId, String productName, double productPrice, String productThumbnail, int productQuantity) {
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productThumbnail = productThumbnail;
        this.productQuantity = productQuantity;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductThumbnail() {
        return productThumbnail;
    }

    public void setProductThumbnail(String productThumbnail) {
        this.productThumbnail = productThumbnail;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }


    @Override
    public String toString() {
        return "CartProductDetails{" +
                "productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productPrice=" + productPrice +
                ", productThumbnail='" + productThumbnail + '\'' +
                ", productQuantity=" + productQuantity +
                '}';
    }
}
