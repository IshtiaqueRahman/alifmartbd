package com.alifmart.cache.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.alifmart.cache.entities.CartProductDetails;

import java.util.List;

@Dao
public interface CartProductDetailsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CartProductDetails cartProductDetails);

    @Query("SELECT * FROM cart_product_details")
    List<CartProductDetails> getAllCartProduct();

    @Query("DELETE FROM cart_product_details WHERE product_id = :productId")
    void deleteCartProduct(int productId);

    @Query("UPDATE cart_product_details SET product_quantity = :productQTY, product_price = :productPrice WHERE product_id = :productID")
    void updateCartProduct(int productQTY, int productPrice, int productID);

}
