package com.alifmart.cache;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.alifmart.cache.dao.CartProductDetailsDao;
import com.alifmart.cache.entities.CartProductDetails;

@Database(entities = {CartProductDetails.class}, version = 1)
public abstract class AlifRoomDatabase extends RoomDatabase {

    public abstract CartProductDetailsDao cartProductDetailsDao();

    private static volatile AlifRoomDatabase INSTANCE;

    public static AlifRoomDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized ((AlifRoomDatabase.class)) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AlifRoomDatabase.class, "alif_mart_db").allowMainThreadQueries().build();
                }
            }
        }
        return INSTANCE;
    }


}
