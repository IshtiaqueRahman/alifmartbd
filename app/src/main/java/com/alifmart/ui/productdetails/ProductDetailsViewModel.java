package com.alifmart.ui.productdetails;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.alifmart.cache.entities.CartProductDetails;
import com.alifmart.models.productdetails.ProductDetailsPojo;

public class ProductDetailsViewModel extends AndroidViewModel {

    private ProductDetailsRepository productDetailsRepository;

    public ProductDetailsViewModel(@NonNull Application application) {
        super(application);
    }

    public void ProductDetailsViewModelInit(Context context) {
        productDetailsRepository = new ProductDetailsRepository(context);
    }

    public MutableLiveData<ProductDetailsPojo> getDetails(int productId) {
        return productDetailsRepository.getProductDetails(productId);
    }

    public void insertCartProduct(CartProductDetails cartProductDetails) {
        productDetailsRepository.insertCartProduct(cartProductDetails);
    }
}