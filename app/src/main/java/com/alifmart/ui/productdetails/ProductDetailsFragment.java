package com.alifmart.ui.productdetails;



import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alifmart.R;
import com.alifmart.adapters.ImageSliderAdapter;
import com.alifmart.cache.AlifRoomDatabase;
import com.alifmart.cache.entities.CartProductDetails;
import com.alifmart.databinding.ProductDetailsFragmentBinding;
import com.alifmart.models.productdetails.Image;
import com.alifmart.models.productdetails.ProductDetailsPojo;
import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

public class ProductDetailsFragment extends Fragment {

    private ProductDetailsViewModel mViewModel;
    private ProductDetailsFragmentBinding productDetailsFragmentBinding;

    private TextView productNameTV, productDescTV, productMetaTitleTV, productMetaDescTV, productPriceTV;

    private Context context;

    private CartProductDetails cartProductDetails;
    private int LOADED = 0;

    public static ProductDetailsFragment newInstance() {
        return new ProductDetailsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        productDetailsFragmentBinding = ProductDetailsFragmentBinding.inflate(inflater, container, false);
        View root = productDetailsFragmentBinding.getRoot();

        mViewModel = new ViewModelProvider(this).get(ProductDetailsViewModel.class);
        mViewModel.ProductDetailsViewModelInit(context);
        int productId = ProductDetailsFragmentArgs.fromBundle(getArguments()).getProductId();


        //get data
        mViewModel.getDetails(productId).observe(getViewLifecycleOwner(), new Observer<ProductDetailsPojo>() {
            @Override
            public void onChanged(ProductDetailsPojo productDetailsPojo) {
                initializations(productDetailsPojo);
                setUpImageSlider(productDetailsPojo.getImages());
                cartProductDetails = new CartProductDetails(productDetailsPojo.getId(), productDetailsPojo.getName(), Double.parseDouble(productDetailsPojo.getUnitPrice()), productDetailsPojo.getThumbnailImages().get(0).getFileName(), 1);
                LOADED = 1;
            }
        });

        //button works
        MaterialButton buyNowBTN = productDetailsFragmentBinding.productDetailsBuynow;
        buyNowBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LOADED == 1) {
                    mViewModel.insertCartProduct(cartProductDetails);
                }

            }
        });

        MaterialButton addToCartBTN = productDetailsFragmentBinding.productDetailsAddtocart;
        addToCartBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LOADED == 1) {
                    mViewModel.insertCartProduct(cartProductDetails);
                }

            }
        });

        return root;
    }

    private void initializations(ProductDetailsPojo productDetailsPojo) {

        productNameTV = productDetailsFragmentBinding.detailsProductName;
        productDescTV = productDetailsFragmentBinding.detailsProductDesc;
        productMetaTitleTV = productDetailsFragmentBinding.productDetailsMetatitle;
        productMetaDescTV = productDetailsFragmentBinding.productDetailsMetadesc;
        productPriceTV = productDetailsFragmentBinding.detailsProductPrice;

        String productName = productDetailsPojo.getName();
        String productDesc = productDetailsPojo.getDescription();
        String productMetaTitle = productDetailsPojo.getMetaTitle();
        String productMetaDesc = productDetailsPojo.getMetaDescription();
        String productPrice = productDetailsPojo.getUnitPrice();

        if (!productName.isEmpty()) {
            productNameTV.setText(productName);
        }

        if (!productDesc.isEmpty()) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                productDescTV.setText(Html.fromHtml(productDesc, Html.FROM_HTML_MODE_COMPACT));
            } else {
                productDescTV.setText(Html.fromHtml(productDesc));
            }

        }

        if (!productMetaTitle.isEmpty()) {
            productMetaTitleTV.setText(productMetaTitle);
        }

        if (!productMetaDesc.isEmpty()) {
            productMetaDescTV.setText(productMetaDesc);
        }

        if (!productPrice.isEmpty()) {
            productPriceTV.setText(productPrice);
        }

    }


    private void setUpImageSlider(List<Image> listImages) {

        List<String> fileNames = new ArrayList<>();

        for (Image image : listImages) {
            fileNames.add(image.getFileName());
        }


        SliderView sliderView = productDetailsFragmentBinding.imageSlider;
        ImageSliderAdapter imageSliderAdapter = new ImageSliderAdapter(fileNames, context);
        sliderView.setSliderAdapter(imageSliderAdapter);
        sliderView.setIndicatorAnimation(IndicatorAnimationType.DROP); //set indicator animation by using IndicatorAnimationType. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        //sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        //sliderView.setScrollTimeInSec(2); //set scroll delay in seconds :

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

}