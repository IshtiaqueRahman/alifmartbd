package com.alifmart.ui.productdetails;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.alifmart.apiutils.ApiClient;
import com.alifmart.apiutils.ApiInterface;
import com.alifmart.cache.AlifRoomDatabase;
import com.alifmart.cache.entities.CartProductDetails;
import com.alifmart.models.productdetails.ProductDetailsPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailsRepository {

    private Context context;
    private ApiClient apiClient;

    public ProductDetailsRepository(Context context) {
        this.context = context;
        apiClient = new ApiClient(context);
    }

    public MutableLiveData<ProductDetailsPojo> getProductDetails(int productId) {
        MutableLiveData<ProductDetailsPojo> productDetails = new MutableLiveData<>();

        ApiInterface apiInterface = apiClient.getClient().create(ApiInterface.class);

        Call<ProductDetailsPojo> detailsPojoCall = apiInterface.getProductDetails(productId);
        detailsPojoCall.enqueue(new Callback<ProductDetailsPojo>() {
            @Override
            public void onResponse(Call<ProductDetailsPojo> call, Response<ProductDetailsPojo> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        productDetails.setValue(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductDetailsPojo> call, Throwable t) {

            }
        });

        return productDetails;
    }

    public void insertCartProduct(CartProductDetails cartProductDetails) {

        AlifRoomDatabase.getInstance(context).cartProductDetailsDao().insert(cartProductDetails);

    }
}
