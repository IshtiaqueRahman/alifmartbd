package com.alifmart.ui.categories;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.alifmart.models.listcategory.CategorySubCategoryPojo;

public class CategoriesViewModel extends AndroidViewModel {

    private CategoriesRepository categoriesRepository;

    public CategoriesViewModel(@NonNull Application application) {
        super(application);
    }

    public void CategoriesViewModelInit(Context context) {
        categoriesRepository = new CategoriesRepository(context);
    }

    public MutableLiveData<CategorySubCategoryPojo> getAllCategorySubCategory() {
        return categoriesRepository.getAllCategories();
    }
}