package com.alifmart.ui.categories;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alifmart.R;
import com.alifmart.adapters.CategoryListAdapter;
import com.alifmart.databinding.CategoriesFragmentBinding;
import com.alifmart.models.listcategory.Category;
import com.alifmart.models.listcategory.CategorySubCategoryPojo;
import com.alifmart.utils.AppUtils;
import com.alifmart.utils.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

public class CategoriesFragment extends Fragment {

    private CategoriesViewModel categoriesViewModel;
    private CategoriesFragmentBinding categoriesFragmentBinding;
    private Context context;
    private AppUtils appUtils;

    public static CategoriesFragment newInstance() {
        return new CategoriesFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        categoriesFragmentBinding = CategoriesFragmentBinding.inflate(inflater, container, false);
        View view = categoriesFragmentBinding.getRoot();

        categoriesViewModel = new ViewModelProvider(this).get(CategoriesViewModel.class);
        appUtils = new AppUtils(context);


        if (appUtils.isOnline()) {

            categoriesViewModel.CategoriesViewModelInit(context);
            categoriesViewModel.getAllCategorySubCategory().observe(getViewLifecycleOwner(), new Observer<CategorySubCategoryPojo>() {
                @Override
                public void onChanged(CategorySubCategoryPojo categorySubCategoryPojo) {
                    //set up rv
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
                    RecyclerView categoryRV = categoriesFragmentBinding.categoriesRv;
                    CategoryListAdapter categoryListAdapter = new CategoryListAdapter(context, categorySubCategoryPojo.getData());
                    categoryRV.setLayoutManager(gridLayoutManager);
                    categoryRV.setAdapter(categoryListAdapter);

                    //click listner
                    categoryRV.addOnItemTouchListener(new RecyclerTouchListener(context, categoryRV, new RecyclerTouchListener.ClickListener() {
                        @Override
                        public void onClick(View view, int position) {
                            if (categorySubCategoryPojo.getData().get(position).getCategories().isEmpty()) {
                                NavDirections navDirections = CategoriesFragmentDirections.actionNavigationCategoriesToNavigationViewAllCategoryProducts(categorySubCategoryPojo.getData().get(position).getId());
                                Navigation.findNavController(view).navigate(navDirections);
                            } else {
                                NavDirections navDirections = CategoriesFragmentDirections.actionNavigationCategoriesToNavigationSubCategory(position);
                                Navigation.findNavController(view).navigate(navDirections);
                            }

                        }

                        @Override
                        public void onLongClick(View view, int position) {

                        }
                    }));
                }
            });

        }


        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }



}