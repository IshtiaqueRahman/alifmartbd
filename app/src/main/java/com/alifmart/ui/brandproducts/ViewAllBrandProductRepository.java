package com.alifmart.ui.brandproducts;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.alifmart.apiutils.ApiClient;
import com.alifmart.apiutils.ApiInterface;
import com.alifmart.models.brandproducts.BrandProductsPojo;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAllBrandProductRepository {

    private Context context;
    private ApiClient apiClient;
    private MutableLiveData<BrandProductsPojo> brandProductsPojoMutableLiveData = new MutableLiveData<>();

    public ViewAllBrandProductRepository(Context context, int brandId) {
        this.context = context;
        apiClient = new ApiClient(context);
        getAllBrandProducts(brandId);

    }

    public void getAllBrandProducts(int brandId){

        ApiInterface apiInterface = apiClient.getClient().create(ApiInterface.class);
        Call<BrandProductsPojo> apiCall = apiInterface.getAllProductsForBrand(brandId);

        apiCall.enqueue(new Callback<BrandProductsPojo>() {
            @Override
            public void onResponse(Call<BrandProductsPojo> call, Response<BrandProductsPojo> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        brandProductsPojoMutableLiveData.setValue(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<BrandProductsPojo> call, Throwable t) {
                Toasty.warning(context, "No products.").show();
            }
        });

    }


    public MutableLiveData<BrandProductsPojo> getBrandProductsPojoMutableLiveData() {
        return brandProductsPojoMutableLiveData;
    }
}
