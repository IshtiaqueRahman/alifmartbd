package com.alifmart.ui.brandproducts;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alifmart.R;
import com.alifmart.adapters.ViewAllProductsAdapter;
import com.alifmart.databinding.ViewAllBrandProductFragmentBinding;
import com.alifmart.models.brandproducts.BrandProductsPojo;
import com.alifmart.ui.home.HomeFragmentDirections;
import com.alifmart.utils.AppUtils;
import com.alifmart.utils.RecyclerTouchListener;

public class ViewAllBrandProductFragment extends Fragment {

    private ViewAllBrandProductViewModel viewAllBrandProductViewModel;
    private ViewAllBrandProductFragmentBinding viewAllBrandProductFragmentBinding;
    private AppUtils appUtils;
    private Context context;
    private int brandId;

    public static ViewAllBrandProductFragment newInstance() {
        return new ViewAllBrandProductFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        viewAllBrandProductFragmentBinding = ViewAllBrandProductFragmentBinding.inflate(inflater, container, false);
        View root = viewAllBrandProductFragmentBinding.getRoot();

        viewAllBrandProductViewModel = new ViewModelProvider(this).get(ViewAllBrandProductViewModel.class);
        appUtils = new AppUtils(context);

        //get brand id
        brandId = ViewAllBrandProductFragmentArgs.fromBundle(getArguments()).getBrandId();

        if (appUtils.isOnline()){
            viewAllBrandProductViewModel.ViewAllBrandProductViewModelInit(context, brandId);

            viewAllBrandProductViewModel.getAllProductsForBrand().observe(getViewLifecycleOwner(), new Observer<BrandProductsPojo>() {
                @Override
                public void onChanged(BrandProductsPojo brandProductsPojo) {
                    // set up recycler view
                    RecyclerView brandProductRV = viewAllBrandProductFragmentBinding.brandAllProductsRv;
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
                    ViewAllProductsAdapter viewAllProductsAdapter = new ViewAllProductsAdapter(context, brandProductsPojo.getData().getProducts());
                    brandProductRV.setLayoutManager(gridLayoutManager);
                    brandProductRV.setAdapter(viewAllProductsAdapter);

                    //onclick item
                    brandProductRV.addOnItemTouchListener(new RecyclerTouchListener(context, brandProductRV, new RecyclerTouchListener.ClickListener() {
                        @Override
                        public void onClick(View view, int position) {

                            int productId = brandProductsPojo.getData().getProducts().get(position).getId();
                            NavDirections directions = ViewAllBrandProductFragmentDirections.actionNavigationViewAllBrandProductsToNavigationProductDetails(productId);
                            Navigation.findNavController(view).navigate(directions);

                        }

                        @Override
                        public void onLongClick(View view, int position) {

                        }
                    }));
                }
            });
        }

        return root;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

}