package com.alifmart.ui.brandproducts;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import com.alifmart.models.brandproducts.BrandProductsPojo;

public class ViewAllBrandProductViewModel extends AndroidViewModel {

    private ViewAllBrandProductRepository viewAllBrandProductRepository;

    public ViewAllBrandProductViewModel(@NonNull Application application) {
        super(application);
    }

    public void ViewAllBrandProductViewModelInit(Context context, int brandId){
        viewAllBrandProductRepository = new ViewAllBrandProductRepository(context, brandId);
    }

    public MutableLiveData<BrandProductsPojo> getAllProductsForBrand(){
        return viewAllBrandProductRepository.getBrandProductsPojoMutableLiveData();
    }

}