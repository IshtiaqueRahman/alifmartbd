package com.alifmart.ui.profile;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.alifmart.databinding.FragmentProfileBinding;
import com.alifmart.models.loginregistration.Login;
import com.alifmart.models.loginregistration.Registration;
import com.alifmart.sharedpreference.PrefManager;
import com.alifmart.ui.createaccount.CreateAccountFragmentDirections;
import com.alifmart.ui.home.HomeFragmentDirections;

import java.util.HashMap;

public class ProfileFragment extends Fragment {

    private Context context;
    private TextView loginTV;
    private EditText emailEdt,passwordEdt;
    private ProfileViewModel profileViewModel;
    private FragmentProfileBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        profileViewModel =
                new ViewModelProvider(this).get(ProfileViewModel.class);

        binding = FragmentProfileBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        initializations();


        return root;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void initializations() {
        loginTV = binding.login;
        emailEdt = binding.email;
        passwordEdt = binding.password;

        loginTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileViewModel.getLoginStatusInit(context,emailEdt.getText().toString(),passwordEdt.getText().toString());
                profileViewModel.getLoginStatus().observe(getViewLifecycleOwner(), new Observer<HashMap<String, String>>() {
                    @Override
                    public void onChanged(HashMap<String, String> stringStringHashMap) {
                        Log.e("status",stringStringHashMap.get("code"));
                    }
                });
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}