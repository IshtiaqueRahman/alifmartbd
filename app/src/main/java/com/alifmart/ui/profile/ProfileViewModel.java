package com.alifmart.ui.profile;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.HashMap;

public class ProfileViewModel extends AndroidViewModel {
    private ProfiletRepository profiletRepository;

    public ProfileViewModel(@NonNull Application application) {
        super(application);
    }

    public void getLoginStatusInit(Context context,String email,String password){
        profiletRepository = new ProfiletRepository(context,email,password);
    }

    public MutableLiveData<HashMap<String, String>> getLoginStatus(){
        return profiletRepository.getLoginPojoMutableLiveData();
    }
}