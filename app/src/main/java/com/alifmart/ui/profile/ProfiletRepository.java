package com.alifmart.ui.profile;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.alifmart.apiutils.ApiClient;
import com.alifmart.apiutils.ApiInterface;
import com.alifmart.models.loginregistration.Login;
import com.alifmart.sharedpreference.PrefManager;

import java.util.HashMap;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfiletRepository {

    private Context context;
    private ApiClient apiClient;
    private MutableLiveData<HashMap<String,String>> loginPojoMutableLiveData = new MutableLiveData<>();

    public ProfiletRepository(Context context, String email, String password) {
        this.context = context;
        apiClient = new ApiClient(context);
        getLoginStatus(email,password);
    }


    public void getLoginStatus(String email, String password){

        ApiInterface apiInterface = apiClient.getClient().create(ApiInterface.class);
        Call<Login> apiCall = apiInterface.getLoginStatus(email,password);

        apiCall.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if (response.isSuccessful()){
                    if (response.code() == 200){
                        new PrefManager(context).saveUserToken(response.body().getToken());
                        HashMap<String,String> responseMap = new HashMap<>();
                        responseMap.put("code","200");
                        responseMap.put("token",response.body().getToken());
                        loginPojoMutableLiveData.setValue(responseMap);
                        //loginPojoMutableLiveData.setValue(response.body().getMessage());
                    }
                    if(response.code() == 400){
                        HashMap<String,String> responseMap = new HashMap<>();
                        responseMap.put("code","400");
                        loginPojoMutableLiveData.setValue(responseMap);
                    }
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Toasty.warning(context, "No products.").show();
            }
        });

    }


    public MutableLiveData<HashMap<String, String>> getLoginPojoMutableLiveData() {
        return loginPojoMutableLiveData;
    }
}
