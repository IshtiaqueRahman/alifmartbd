package com.alifmart.ui.createaccount;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.alifmart.databinding.CreateAccountFragmentBinding;
import com.alifmart.models.loginregistration.Registration;
import com.alifmart.ui.profile.ProfileFragmentDirections;

public class CreateAccountFragment extends Fragment {

    private Context context;
    private EditText nameEdt,emailEdt,phoneEdt,passwordEdt,confirmEdt;
    private Button registerBtn;
    private CreateAccountFragmentBinding binding;
    private CreateAccountViewModel createAccountViewModel;

    public static CreateAccountFragment newInstance() {
        return new CreateAccountFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        createAccountViewModel =
                new ViewModelProvider(this).get(CreateAccountViewModel.class);

        binding = CreateAccountFragmentBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        initializations();
        return root;
    }

    private void initializations() {
        nameEdt = binding.name;
        emailEdt = binding.email;
        phoneEdt = binding.phone;
        passwordEdt = binding.password;
        confirmEdt = binding.cpassword;
        registerBtn = binding.register;
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameEdt.getText().toString();
                String email = emailEdt.getText().toString();
                String phone = phoneEdt.getText().toString();
                String password = passwordEdt.getText().toString();
                String confirmpassword = confirmEdt.getText().toString();

                createAccountViewModel.RegistrationViewModelInit(context,name,email,phone,password,confirmpassword);
                createAccountViewModel.getRegistrationStatus().observe(getViewLifecycleOwner(), new Observer<Registration>() {
                    @Override
                    public void onChanged(Registration registration) {
                        Log.e("accestoken",registration.getAccessToken());
                        if(!registration.getAccessToken().equals("")){
                            Navigation.findNavController(v).navigate(CreateAccountFragmentDirections.actionNavigationCreateAccountToNavigationProfile());
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}