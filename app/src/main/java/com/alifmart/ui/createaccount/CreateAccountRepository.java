package com.alifmart.ui.createaccount;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.alifmart.apiutils.ApiClient;
import com.alifmart.apiutils.ApiInterface;
import com.alifmart.models.loginregistration.Registration;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateAccountRepository {

    private Context context;
    private ApiClient apiClient;
    private MutableLiveData<Registration> registratationPojoMutableLiveData = new MutableLiveData<>();

    public CreateAccountRepository(Context context, String name, String email, String phone, String password) {
        this.context = context;
        apiClient = new ApiClient(context);
        getLoginStatus(name,email,phone,password);
    }


    public void getLoginStatus(String name, String email, String phone, String password){

        ApiInterface apiInterface = apiClient.getClient().create(ApiInterface.class);
        Call<Registration> apiCall = apiInterface.getUserRegistrationStatus(name,email,phone,password);

        apiCall.enqueue(new Callback<Registration>() {
            @Override
            public void onResponse(Call<Registration> call, Response<Registration> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        registratationPojoMutableLiveData.setValue(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<Registration> call, Throwable t) {
                Toasty.warning(context, "No products.").show();
            }
        });

    }


    public MutableLiveData<Registration> getRegistrationPojoMutableLiveData() {
        return registratationPojoMutableLiveData;
    }
}
