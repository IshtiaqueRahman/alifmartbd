package com.alifmart.ui.createaccount;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.alifmart.models.loginregistration.Registration;


public class CreateAccountViewModel extends AndroidViewModel {
    // TODO: Implement the ViewModel
    private CreateAccountRepository createAccountRepository;

    public CreateAccountViewModel(@NonNull Application application) {
        super(application);
    }

    public void RegistrationViewModelInit(Context context, String name, String email, String phone, String password, String confirmpassword) {
        createAccountRepository = new CreateAccountRepository(context,name,email,phone,password);
    }

    public MutableLiveData<Registration> getRegistrationStatus() {
        return createAccountRepository.getRegistrationPojoMutableLiveData();
    }
}