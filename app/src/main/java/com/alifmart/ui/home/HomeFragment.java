package com.alifmart.ui.home;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alifmart.R;
import com.alifmart.adapters.HomeParentRecyclerAdapter;
import com.alifmart.adapters.ImageSliderAdapter;
import com.alifmart.databinding.FragmentHomeBinding;
import com.alifmart.models.Datum;
import com.alifmart.models.ProductsPojo;
import com.alifmart.models.brands.Brand;
import com.alifmart.utils.AppUtils;
import com.alifmart.utils.Loader;
import com.alifmart.utils.TransparentProgressDialog;
import com.google.android.material.card.MaterialCardView;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private Context context;
    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;
    private ImageSliderAdapter imageSliderAdapter;
    private AppUtils appUtils;
    private Loader loader;
    private TransparentProgressDialog transparentProgressDialog;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        //app utils initialization
        appUtils = new AppUtils(context);
        loader = new Loader(context);
        transparentProgressDialog = new TransparentProgressDialog(context, R.drawable.loader);

        //viewmodel initialization
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        homeViewModel.HomeViewModelInit(context);


        //transparentProgressDialog.show();

        if (appUtils.isOnline()) {
            //do nothing
        } else {
            Toast.makeText(context, "No Internet Connection!", Toast.LENGTH_SHORT).show();
        }


        //get slider images into views
        homeViewModel.getListSliderImages().observe(getViewLifecycleOwner(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> data) {
                //load image slider
                setUpImageSlider(data);
            }
        });

        //get all products
        homeViewModel.getAllProducts().observe(getViewLifecycleOwner(), new Observer<ProductsPojo>() {
            @Override
            public void onChanged(ProductsPojo productsPojo) {
                if (productsPojo != null) {
                    //List<Brand> brands = new ArrayList<>();
                    //brands.add(productsPojo.getBrands().get(0));
                    //brands.add(productsPojo.getBrands().get(1));
                    RecyclerView homeParentRV = binding.homeProductsRv;
                    LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                    HomeParentRecyclerAdapter homeParentRecyclerAdapter = new HomeParentRecyclerAdapter(context, productsPojo.getBrands());
                    homeParentRV.setLayoutManager(layoutManager);
                    homeParentRV.setAdapter(homeParentRecyclerAdapter);


                }
            }
        });


        //category on click
        MaterialCardView categoryCardView = binding.homeCategoryCard;

        categoryCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(HomeFragmentDirections.actionNavigationHomeToNavigationCategories());
            }
        });

        /*Handler handler = new Handler();
        handler.postDelayed(() -> transparentProgressDialog.dismiss(), 3000);*/

        return root;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void setUpImageSlider(List<String> listImages) {

        SliderView sliderView = binding.imageSlider;
        imageSliderAdapter = new ImageSliderAdapter(listImages, context);
        sliderView.setSliderAdapter(imageSliderAdapter);
        sliderView.setIndicatorAnimation(IndicatorAnimationType.DROP); //set indicator animation by using IndicatorAnimationType. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(2); //set scroll delay in seconds :
        sliderView.startAutoCycle();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}