package com.alifmart.ui.home;

import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.alifmart.apiutils.ApiClient;
import com.alifmart.apiutils.ApiInterface;
import com.alifmart.models.Datum;
import com.alifmart.models.ProductsPojo;
import com.alifmart.models.SliderImagesPojo;
import com.alifmart.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeRepository {

    private Context context;
    private ApiClient apiClient;
    private final MutableLiveData<ProductsPojo> productsPojoMutableLiveData = new MutableLiveData<>();

    public HomeRepository(Context context) {
        this.context = context;
        apiClient = new ApiClient(context);
        allProductList();
    }

    //get slider images
    public MutableLiveData<List<String>> getImagesForSlider() {
        MutableLiveData<List<String>> listMutableLiveData = new MutableLiveData<>();

        ApiInterface apiInterface = apiClient.getClient().create(ApiInterface.class);
        Call<SliderImagesPojo> call = apiInterface.getSliderImages();
        call.enqueue(new Callback<SliderImagesPojo>() {
            @Override
            public void onResponse(Call<SliderImagesPojo> call, Response<SliderImagesPojo> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        List<Datum> allData = new ArrayList<>(response.body().getData());
                        List<String> tempValue = new ArrayList<>();

                        for (Datum datum : allData) {
                            tempValue.add(datum.getFileName());
                        }

                        listMutableLiveData.setValue(tempValue);
                    }
                }
            }

            @Override
            public void onFailure(Call<SliderImagesPojo> call, Throwable t) {

            }
        });

        return listMutableLiveData;
    }


    //get all products
    public void allProductList(){
        ApiInterface apiInterface = apiClient.getClient().create(ApiInterface.class);
        Call<ProductsPojo> productsPojoCall = apiInterface.getAllProducts();
        productsPojoCall.enqueue(new Callback<ProductsPojo>() {
            @Override
            public void onResponse(Call<ProductsPojo> call, Response<ProductsPojo> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        productsPojoMutableLiveData.setValue(response.body());
                        Toasty.info(context, "Success", Toasty.LENGTH_LONG, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductsPojo> call, Throwable t) {

            }
        });
    }

    public MutableLiveData<ProductsPojo> getProductsPojoMutableLiveData() {
        return productsPojoMutableLiveData;
    }
}
