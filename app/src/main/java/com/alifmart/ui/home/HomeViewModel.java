package com.alifmart.ui.home;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.alifmart.models.Datum;
import com.alifmart.models.ProductsPojo;

import java.util.List;

public class HomeViewModel extends AndroidViewModel {

    private HomeRepository homeRepository;

    public HomeViewModel(@NonNull Application application) {
        super(application);
    }

    //initialize
    public void HomeViewModelInit(Context context){
        homeRepository = new HomeRepository(context);
    }

    public MutableLiveData<List<String>> getListSliderImages() {
        return homeRepository.getImagesForSlider();
    }

    public MutableLiveData<ProductsPojo> getAllProducts(){
        return homeRepository.getProductsPojoMutableLiveData();
    }
}