package com.alifmart.ui.subcategories;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.alifmart.models.listcategory.CategorySubCategoryPojo;

public class SubCategoriesViewModel extends AndroidViewModel {

    private SubCategoryRepositry subCategoryRepositry;

    public SubCategoriesViewModel(@NonNull Application application) {
        super(application);
    }

    public void SubCategoriesViewModelInit(Context context) {
        subCategoryRepositry = new SubCategoryRepositry(context);
    }

    public MutableLiveData<CategorySubCategoryPojo> getAllSubCategory() {
        return subCategoryRepositry.getSubCategories();
    }
}
