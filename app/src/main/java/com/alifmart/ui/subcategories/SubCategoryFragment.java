package com.alifmart.ui.subcategories;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alifmart.R;
import com.alifmart.adapters.CategoryListAdapter;
import com.alifmart.adapters.SubCategoryListAdapter;
import com.alifmart.databinding.FragmentSubCategoryBinding;
import com.alifmart.models.listcategory.CategorySubCategoryPojo;
import com.alifmart.ui.categories.CategoriesFragmentDirections;
import com.alifmart.utils.AppUtils;
import com.alifmart.utils.RecyclerTouchListener;

public class SubCategoryFragment extends Fragment {

    private FragmentSubCategoryBinding fragmentSubCategoryBinding;
    private SubCategoriesViewModel subCategoriesViewModel;
    private AppUtils appUtils;
    private Context context;
    private int positionOfCategory = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentSubCategoryBinding = FragmentSubCategoryBinding.inflate(inflater, container, false);
        View root = fragmentSubCategoryBinding.getRoot();

        subCategoriesViewModel = new ViewModelProvider(this).get(SubCategoriesViewModel.class);
        appUtils = new AppUtils(context);

        if (appUtils.isOnline()) {

            positionOfCategory = SubCategoryFragmentArgs.fromBundle(getArguments()).getPosition();

            if (positionOfCategory != -1) {
                subCategoriesViewModel.SubCategoriesViewModelInit(context);
                subCategoriesViewModel.getAllSubCategory().observe(getViewLifecycleOwner(), new Observer<CategorySubCategoryPojo>() {
                    @Override
                    public void onChanged(CategorySubCategoryPojo categorySubCategoryPojo) {

                        //set up rv
                        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
                        RecyclerView subCategoryRV = fragmentSubCategoryBinding.subCategoriesRv;
                        SubCategoryListAdapter subCategoryListAdapter = new SubCategoryListAdapter(context, categorySubCategoryPojo.getData().get(positionOfCategory).getCategories());
                        subCategoryRV.setLayoutManager(gridLayoutManager);
                        subCategoryRV.setAdapter(subCategoryListAdapter);

                        //click handle
                        subCategoryRV.addOnItemTouchListener(new RecyclerTouchListener(context, subCategoryRV, new RecyclerTouchListener.ClickListener() {
                            @Override
                            public void onClick(View view, int position) {
                                NavDirections navDirections = SubCategoryFragmentDirections.actionNavigationSubCategoryToNavigationViewAllCategoryProducts(categorySubCategoryPojo.getData().get(positionOfCategory).getCategories().get(position).getId());
                                Navigation.findNavController(view).navigate(navDirections);
                            }

                            @Override
                            public void onLongClick(View view, int position) {

                            }
                        }));

                    }
                });
            }


        }

        return root;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}