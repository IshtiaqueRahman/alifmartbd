package com.alifmart.ui.subcategories;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.alifmart.apiutils.ApiClient;
import com.alifmart.apiutils.ApiInterface;
import com.alifmart.models.listcategory.CategorySubCategoryPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubCategoryRepositry {

    private Context context;
    private MutableLiveData<CategorySubCategoryPojo> subCategories = new MutableLiveData<>();
    private ApiClient apiClient;

    public SubCategoryRepositry(Context context) {
        this.context = context;
        apiClient = new ApiClient(context);
        getAllSubCategories();
    }

    private void getAllSubCategories() {
        ApiInterface apiInterface = apiClient.getClient().create(ApiInterface.class);
        Call<CategorySubCategoryPojo> getCall = apiInterface.getAllCategorySubCategory();
        getCall.enqueue(new Callback<CategorySubCategoryPojo>() {
            @Override
            public void onResponse(Call<CategorySubCategoryPojo> call, Response<CategorySubCategoryPojo> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        subCategories.setValue(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<CategorySubCategoryPojo> call, Throwable t) {

            }
        });
    }

    public MutableLiveData<CategorySubCategoryPojo> getSubCategories() {
        return subCategories;
    }
}
