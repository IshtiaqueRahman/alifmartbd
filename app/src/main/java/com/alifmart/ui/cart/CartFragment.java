package com.alifmart.ui.cart;

import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alifmart.R;
import com.alifmart.adapters.CartRecyclerAdapter;
import com.alifmart.cache.AlifRoomDatabase;
import com.alifmart.cache.entities.CartProductDetails;
import com.alifmart.databinding.CartFragmentBinding;

import java.util.List;

public class CartFragment extends Fragment {

    private CartViewModel mViewModel;
    private CartFragmentBinding cartFragmentBinding;
    private Context context;

    public static CartFragment newInstance() {
        return new CartFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        cartFragmentBinding = CartFragmentBinding.inflate(inflater, container, false);
        View root = cartFragmentBinding.getRoot();

        mViewModel = new ViewModelProvider(this).get(CartViewModel.class);

        List<CartProductDetails> cartProductDetailsList = AlifRoomDatabase.getInstance(context).cartProductDetailsDao().getAllCartProduct();

        Log.e("Cart......", cartProductDetailsList.toString());

        RecyclerView cartRV = cartFragmentBinding.cartOrderListRv;
        LinearLayoutManager manager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        cartRV.setLayoutManager(manager);
        CartRecyclerAdapter cartRecyclerAdapter = new CartRecyclerAdapter(context, cartProductDetailsList);
        cartRV.setAdapter(cartRecyclerAdapter);


        return root;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

}