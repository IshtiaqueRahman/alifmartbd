package com.alifmart.ui.categoryproducts;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.alifmart.models.categoryproducts.CategoryProductsPojo;

public class ViewAllCategoryProductViewModel extends AndroidViewModel {

    private ViewAllCategoryProductRepository viewAllCategoryProductRepository;

    public ViewAllCategoryProductViewModel(@NonNull Application application) {
        super(application);
    }

    public void ViewAllCategoryProductViewModelInit(Context context, int categoryId){
        viewAllCategoryProductRepository = new ViewAllCategoryProductRepository(context, categoryId);
    }

    public MutableLiveData<CategoryProductsPojo> getAllProductsByCategory(){
        return viewAllCategoryProductRepository.getCategoryProductsPojoMutableLiveData();
    }


}