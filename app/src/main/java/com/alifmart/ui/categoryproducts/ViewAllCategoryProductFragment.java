package com.alifmart.ui.categoryproducts;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alifmart.R;
import com.alifmart.adapters.ViewAllProductsByCategoryAdapter;
import com.alifmart.databinding.ViewAllBrandProductFragmentBinding;
import com.alifmart.databinding.ViewAllCategoryProductFragmentBinding;
import com.alifmart.models.categoryproducts.CategoryProductsPojo;
import com.alifmart.utils.AppUtils;

public class ViewAllCategoryProductFragment extends Fragment {

    private ViewAllCategoryProductViewModel viewAllCategoryProductViewModel;
    private ViewAllCategoryProductFragmentBinding viewAllCategoryProductFragmentBinding;

    private AppUtils appUtils;
    private Context context;
    private int categoryId;

    public static ViewAllCategoryProductFragment newInstance() {
        return new ViewAllCategoryProductFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        viewAllCategoryProductFragmentBinding = ViewAllCategoryProductFragmentBinding.inflate(inflater, container, false);
        View view = viewAllCategoryProductFragmentBinding.getRoot();

        viewAllCategoryProductViewModel = new ViewModelProvider(this).get(ViewAllCategoryProductViewModel.class);
        appUtils = new AppUtils(context);

        categoryId = ViewAllCategoryProductFragmentArgs.fromBundle(getArguments()).getCategoryId();

        if (appUtils.isOnline()){

            viewAllCategoryProductViewModel.ViewAllCategoryProductViewModelInit(context, categoryId);

            viewAllCategoryProductViewModel.getAllProductsByCategory().observe(getViewLifecycleOwner(), new Observer<CategoryProductsPojo>() {
                @Override
                public void onChanged(CategoryProductsPojo categoryProductsPojo) {
                    RecyclerView categoryProductRV = viewAllCategoryProductFragmentBinding.categoryAllProductsRv;
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
                    ViewAllProductsByCategoryAdapter adapter = new ViewAllProductsByCategoryAdapter(context, categoryProductsPojo.getData().getProducts());
                    categoryProductRV.setLayoutManager(gridLayoutManager);
                    categoryProductRV.setAdapter(adapter);
                }
            });

        }

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }


}