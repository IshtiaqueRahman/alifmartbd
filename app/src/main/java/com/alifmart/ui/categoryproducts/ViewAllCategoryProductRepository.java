package com.alifmart.ui.categoryproducts;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.alifmart.apiutils.ApiClient;
import com.alifmart.apiutils.ApiInterface;
import com.alifmart.models.categoryproducts.CategoryProductsPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAllCategoryProductRepository {

    private Context context;
    private ApiClient apiClient;
    private MutableLiveData<CategoryProductsPojo> categoryProductsPojoMutableLiveData = new MutableLiveData<>();

    public ViewAllCategoryProductRepository(Context context, int categoryId) {
        this.context = context;
        apiClient = new ApiClient(context);
        getAllCategoryProducts(categoryId);
    }

    private void getAllCategoryProducts(int categoryId) {
        ApiInterface apiInterface = apiClient.getClient().create(ApiInterface.class);
        Call<CategoryProductsPojo> productsPojoCall = apiInterface.getAllProductsForCategory(categoryId);

        productsPojoCall.enqueue(new Callback<CategoryProductsPojo>() {
            @Override
            public void onResponse(Call<CategoryProductsPojo> call, Response<CategoryProductsPojo> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        categoryProductsPojoMutableLiveData.setValue(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<CategoryProductsPojo> call, Throwable t) {

            }
        });
    }

    public MutableLiveData<CategoryProductsPojo> getCategoryProductsPojoMutableLiveData() {
        return categoryProductsPojoMutableLiveData;
    }
}
