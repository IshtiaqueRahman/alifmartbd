
package com.alifmart.models.productdetails;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ProductDetailsPojo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("added_by")
    @Expose
    private String addedBy;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("brand_id")
    @Expose
    private String brandId;
    @SerializedName("photos")
    @Expose
    private String photos;
    @SerializedName("thumbnail_img")
    @Expose
    private String thumbnailImg;
    @SerializedName("video_provider")
    @Expose
    private String videoProvider;
    @SerializedName("video_link")
    @Expose
    private Object videoLink;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("unit_price")
    @Expose
    private String unitPrice;
    @SerializedName("purchase_price")
    @Expose
    private Object purchasePrice;
    @SerializedName("variant_product")
    @Expose
    private String variantProduct;
    @SerializedName("attributes")
    @Expose
    private String attributes;
    @SerializedName("choice_options")
    @Expose
    private String choiceOptions;
    @SerializedName("colors")
    @Expose
    private String colors;
    @SerializedName("variations")
    @Expose
    private Object variations;
    @SerializedName("todays_deal")
    @Expose
    private String todaysDeal;
    @SerializedName("published")
    @Expose
    private String published;
    @SerializedName("approved")
    @Expose
    private String approved;
    @SerializedName("stock_visibility_state")
    @Expose
    private String stockVisibilityState;
    @SerializedName("cash_on_delivery")
    @Expose
    private String cashOnDelivery;
    @SerializedName("featured")
    @Expose
    private String featured;
    @SerializedName("seller_featured")
    @Expose
    private String sellerFeatured;
    @SerializedName("current_stock")
    @Expose
    private String currentStock;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("min_qty")
    @Expose
    private String minQty;
    @SerializedName("low_stock_quantity")
    @Expose
    private String lowStockQuantity;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("discount_type")
    @Expose
    private String discountType;
    @SerializedName("discount_start_date")
    @Expose
    private Object discountStartDate;
    @SerializedName("discount_end_date")
    @Expose
    private Object discountEndDate;
    @SerializedName("tax")
    @Expose
    private Object tax;
    @SerializedName("tax_type")
    @Expose
    private Object taxType;
    @SerializedName("shipping_type")
    @Expose
    private String shippingType;
    @SerializedName("shipping_cost")
    @Expose
    private String shippingCost;
    @SerializedName("is_quantity_multiplied")
    @Expose
    private String isQuantityMultiplied;
    @SerializedName("est_shipping_days")
    @Expose
    private Object estShippingDays;
    @SerializedName("num_of_sale")
    @Expose
    private String numOfSale;
    @SerializedName("meta_title")
    @Expose
    private String metaTitle;
    @SerializedName("meta_description")
    @Expose
    private String metaDescription;
    @SerializedName("meta_img")
    @Expose
    private String metaImg;
    @SerializedName("pdf")
    @Expose
    private Object pdf;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("barcode")
    @Expose
    private Object barcode;
    @SerializedName("digital")
    @Expose
    private String digital;
    @SerializedName("auction_product")
    @Expose
    private String auctionProduct;
    @SerializedName("file_name")
    @Expose
    private Object fileName;
    @SerializedName("file_path")
    @Expose
    private Object filePath;
    @SerializedName("external_link")
    @Expose
    private Object externalLink;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("images")
    @Expose
    private List<Image> images = null;
    @SerializedName("thumbnail_images")
    @Expose
    private List<ThumbnailImage> thumbnailImages = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public String getThumbnailImg() {
        return thumbnailImg;
    }

    public void setThumbnailImg(String thumbnailImg) {
        this.thumbnailImg = thumbnailImg;
    }

    public String getVideoProvider() {
        return videoProvider;
    }

    public void setVideoProvider(String videoProvider) {
        this.videoProvider = videoProvider;
    }

    public Object getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(Object videoLink) {
        this.videoLink = videoLink;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Object getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(Object purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getVariantProduct() {
        return variantProduct;
    }

    public void setVariantProduct(String variantProduct) {
        this.variantProduct = variantProduct;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getChoiceOptions() {
        return choiceOptions;
    }

    public void setChoiceOptions(String choiceOptions) {
        this.choiceOptions = choiceOptions;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public Object getVariations() {
        return variations;
    }

    public void setVariations(Object variations) {
        this.variations = variations;
    }

    public String getTodaysDeal() {
        return todaysDeal;
    }

    public void setTodaysDeal(String todaysDeal) {
        this.todaysDeal = todaysDeal;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getApproved() {
        return approved;
    }

    public void setApproved(String approved) {
        this.approved = approved;
    }

    public String getStockVisibilityState() {
        return stockVisibilityState;
    }

    public void setStockVisibilityState(String stockVisibilityState) {
        this.stockVisibilityState = stockVisibilityState;
    }

    public String getCashOnDelivery() {
        return cashOnDelivery;
    }

    public void setCashOnDelivery(String cashOnDelivery) {
        this.cashOnDelivery = cashOnDelivery;
    }

    public String getFeatured() {
        return featured;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }

    public String getSellerFeatured() {
        return sellerFeatured;
    }

    public void setSellerFeatured(String sellerFeatured) {
        this.sellerFeatured = sellerFeatured;
    }

    public String getCurrentStock() {
        return currentStock;
    }

    public void setCurrentStock(String currentStock) {
        this.currentStock = currentStock;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getMinQty() {
        return minQty;
    }

    public void setMinQty(String minQty) {
        this.minQty = minQty;
    }

    public String getLowStockQuantity() {
        return lowStockQuantity;
    }

    public void setLowStockQuantity(String lowStockQuantity) {
        this.lowStockQuantity = lowStockQuantity;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public Object getDiscountStartDate() {
        return discountStartDate;
    }

    public void setDiscountStartDate(Object discountStartDate) {
        this.discountStartDate = discountStartDate;
    }

    public Object getDiscountEndDate() {
        return discountEndDate;
    }

    public void setDiscountEndDate(Object discountEndDate) {
        this.discountEndDate = discountEndDate;
    }

    public Object getTax() {
        return tax;
    }

    public void setTax(Object tax) {
        this.tax = tax;
    }

    public Object getTaxType() {
        return taxType;
    }

    public void setTaxType(Object taxType) {
        this.taxType = taxType;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public String getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(String shippingCost) {
        this.shippingCost = shippingCost;
    }

    public String getIsQuantityMultiplied() {
        return isQuantityMultiplied;
    }

    public void setIsQuantityMultiplied(String isQuantityMultiplied) {
        this.isQuantityMultiplied = isQuantityMultiplied;
    }

    public Object getEstShippingDays() {
        return estShippingDays;
    }

    public void setEstShippingDays(Object estShippingDays) {
        this.estShippingDays = estShippingDays;
    }

    public String getNumOfSale() {
        return numOfSale;
    }

    public void setNumOfSale(String numOfSale) {
        this.numOfSale = numOfSale;
    }

    public String getMetaTitle() {
        return metaTitle;
    }

    public void setMetaTitle(String metaTitle) {
        this.metaTitle = metaTitle;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getMetaImg() {
        return metaImg;
    }

    public void setMetaImg(String metaImg) {
        this.metaImg = metaImg;
    }

    public Object getPdf() {
        return pdf;
    }

    public void setPdf(Object pdf) {
        this.pdf = pdf;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Object getBarcode() {
        return barcode;
    }

    public void setBarcode(Object barcode) {
        this.barcode = barcode;
    }

    public String getDigital() {
        return digital;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getAuctionProduct() {
        return auctionProduct;
    }

    public void setAuctionProduct(String auctionProduct) {
        this.auctionProduct = auctionProduct;
    }

    public Object getFileName() {
        return fileName;
    }

    public void setFileName(Object fileName) {
        this.fileName = fileName;
    }

    public Object getFilePath() {
        return filePath;
    }

    public void setFilePath(Object filePath) {
        this.filePath = filePath;
    }

    public Object getExternalLink() {
        return externalLink;
    }

    public void setExternalLink(Object externalLink) {
        this.externalLink = externalLink;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<ThumbnailImage> getThumbnailImages() {
        return thumbnailImages;
    }

    public void setThumbnailImages(List<ThumbnailImage> thumbnailImages) {
        this.thumbnailImages = thumbnailImages;
    }

}
