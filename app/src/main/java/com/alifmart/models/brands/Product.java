package com.alifmart.models.brands;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Product {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("added_by")
    @Expose
    private String addedBy;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("brand_id")
    @Expose
    private Integer brandId;
    @SerializedName("photos")
    @Expose
    private Object photos;
    @SerializedName("thumbnail_img")
    @Expose
    private Object thumbnailImg;
    @SerializedName("video_provider")
    @Expose
    private String videoProvider;
    @SerializedName("video_link")
    @Expose
    private String videoLink;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("unit_price")
    @Expose
    private Integer unitPrice;
    @SerializedName("purchase_price")
    @Expose
    private Object purchasePrice;
    @SerializedName("variant_product")
    @Expose
    private Integer variantProduct;
    @SerializedName("attributes")
    @Expose
    private String attributes;
    @SerializedName("choice_options")
    @Expose
    private String choiceOptions;
    @SerializedName("colors")
    @Expose
    private String colors;
    @SerializedName("variations")
    @Expose
    private Object variations;
    @SerializedName("todays_deal")
    @Expose
    private Integer todaysDeal;
    @SerializedName("published")
    @Expose
    private Integer published;
    @SerializedName("approved")
    @Expose
    private Integer approved;
    @SerializedName("stock_visibility_state")
    @Expose
    private String stockVisibilityState;
    @SerializedName("cash_on_delivery")
    @Expose
    private Integer cashOnDelivery;
    @SerializedName("featured")
    @Expose
    private Integer featured;
    @SerializedName("seller_featured")
    @Expose
    private Integer sellerFeatured;
    @SerializedName("current_stock")
    @Expose
    private Integer currentStock;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("min_qty")
    @Expose
    private Integer minQty;
    @SerializedName("low_stock_quantity")
    @Expose
    private Integer lowStockQuantity;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("discount_type")
    @Expose
    private String discountType;
    @SerializedName("discount_start_date")
    @Expose
    private Object discountStartDate;
    @SerializedName("discount_end_date")
    @Expose
    private Object discountEndDate;
    @SerializedName("tax")
    @Expose
    private Object tax;
    @SerializedName("tax_type")
    @Expose
    private Object taxType;
    @SerializedName("shipping_type")
    @Expose
    private String shippingType;
    @SerializedName("shipping_cost")
    @Expose
    private String shippingCost;
    @SerializedName("is_quantity_multiplied")
    @Expose
    private Integer isQuantityMultiplied;
    @SerializedName("est_shipping_days")
    @Expose
    private Object estShippingDays;
    @SerializedName("num_of_sale")
    @Expose
    private Integer numOfSale;
    @SerializedName("meta_title")
    @Expose
    private String metaTitle;
    @SerializedName("meta_description")
    @Expose
    private String metaDescription;
    @SerializedName("meta_img")
    @Expose
    private Object metaImg;
    @SerializedName("pdf")
    @Expose
    private Object pdf;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("rating")
    @Expose
    private Integer rating;
    @SerializedName("barcode")
    @Expose
    private Object barcode;
    @SerializedName("digital")
    @Expose
    private Integer digital;
    @SerializedName("auction_product")
    @Expose
    private Integer auctionProduct;
    @SerializedName("file_name")
    @Expose
    private Object fileName;
    @SerializedName("file_path")
    @Expose
    private Object filePath;
    @SerializedName("external_link")
    @Expose
    private Object externalLink;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    //            @SerializedName("product_translations")
//            @Expose
//            private List<ProductTranslation> productTranslations = null;
//            @SerializedName("taxes")
//            @Expose
//            private List<Tax> taxes = null;
    @SerializedName("thumbnail_images")
    @Expose
    private List<ThumbnailImage> thumbnail_images;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Object getPhotos() {
        return photos;
    }

    public void setPhotos(Object photos) {
        this.photos = photos;
    }

    public Object getThumbnailImg() {
        return thumbnailImg;
    }

    public void setThumbnailImg(Object thumbnailImg) {
        this.thumbnailImg = thumbnailImg;
    }

    public String getVideoProvider() {
        return videoProvider;
    }

    public void setVideoProvider(String videoProvider) {
        this.videoProvider = videoProvider;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Integer unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Object getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(Object purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public Integer getVariantProduct() {
        return variantProduct;
    }

    public void setVariantProduct(Integer variantProduct) {
        this.variantProduct = variantProduct;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getChoiceOptions() {
        return choiceOptions;
    }

    public void setChoiceOptions(String choiceOptions) {
        this.choiceOptions = choiceOptions;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public Object getVariations() {
        return variations;
    }

    public void setVariations(Object variations) {
        this.variations = variations;
    }

    public Integer getTodaysDeal() {
        return todaysDeal;
    }

    public void setTodaysDeal(Integer todaysDeal) {
        this.todaysDeal = todaysDeal;
    }

    public Integer getPublished() {
        return published;
    }

    public void setPublished(Integer published) {
        this.published = published;
    }

    public Integer getApproved() {
        return approved;
    }

    public void setApproved(Integer approved) {
        this.approved = approved;
    }

    public String getStockVisibilityState() {
        return stockVisibilityState;
    }

    public void setStockVisibilityState(String stockVisibilityState) {
        this.stockVisibilityState = stockVisibilityState;
    }

    public Integer getCashOnDelivery() {
        return cashOnDelivery;
    }

    public void setCashOnDelivery(Integer cashOnDelivery) {
        this.cashOnDelivery = cashOnDelivery;
    }

    public Integer getFeatured() {
        return featured;
    }

    public void setFeatured(Integer featured) {
        this.featured = featured;
    }

    public Integer getSellerFeatured() {
        return sellerFeatured;
    }

    public void setSellerFeatured(Integer sellerFeatured) {
        this.sellerFeatured = sellerFeatured;
    }

    public Integer getCurrentStock() {
        return currentStock;
    }

    public void setCurrentStock(Integer currentStock) {
        this.currentStock = currentStock;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getMinQty() {
        return minQty;
    }

    public void setMinQty(Integer minQty) {
        this.minQty = minQty;
    }

    public Integer getLowStockQuantity() {
        return lowStockQuantity;
    }

    public void setLowStockQuantity(Integer lowStockQuantity) {
        this.lowStockQuantity = lowStockQuantity;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public Object getDiscountStartDate() {
        return discountStartDate;
    }

    public void setDiscountStartDate(Object discountStartDate) {
        this.discountStartDate = discountStartDate;
    }

    public Object getDiscountEndDate() {
        return discountEndDate;
    }

    public void setDiscountEndDate(Object discountEndDate) {
        this.discountEndDate = discountEndDate;
    }

    public Object getTax() {
        return tax;
    }

    public void setTax(Object tax) {
        this.tax = tax;
    }

    public Object getTaxType() {
        return taxType;
    }

    public void setTaxType(Object taxType) {
        this.taxType = taxType;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public String getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(String shippingCost) {
        this.shippingCost = shippingCost;
    }

    public Integer getIsQuantityMultiplied() {
        return isQuantityMultiplied;
    }

    public void setIsQuantityMultiplied(Integer isQuantityMultiplied) {
        this.isQuantityMultiplied = isQuantityMultiplied;
    }

    public Object getEstShippingDays() {
        return estShippingDays;
    }

    public void setEstShippingDays(Object estShippingDays) {
        this.estShippingDays = estShippingDays;
    }

    public Integer getNumOfSale() {
        return numOfSale;
    }

    public void setNumOfSale(Integer numOfSale) {
        this.numOfSale = numOfSale;
    }

    public String getMetaTitle() {
        return metaTitle;
    }

    public void setMetaTitle(String metaTitle) {
        this.metaTitle = metaTitle;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public Object getMetaImg() {
        return metaImg;
    }

    public void setMetaImg(Object metaImg) {
        this.metaImg = metaImg;
    }

    public Object getPdf() {
        return pdf;
    }

    public void setPdf(Object pdf) {
        this.pdf = pdf;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Object getBarcode() {
        return barcode;
    }

    public void setBarcode(Object barcode) {
        this.barcode = barcode;
    }

    public Integer getDigital() {
        return digital;
    }

    public void setDigital(Integer digital) {
        this.digital = digital;
    }

    public Integer getAuctionProduct() {
        return auctionProduct;
    }

    public void setAuctionProduct(Integer auctionProduct) {
        this.auctionProduct = auctionProduct;
    }

    public Object getFileName() {
        return fileName;
    }

    public void setFileName(Object fileName) {
        this.fileName = fileName;
    }

    public Object getFilePath() {
        return filePath;
    }

    public void setFilePath(Object filePath) {
        this.filePath = filePath;
    }

    public Object getExternalLink() {
        return externalLink;
    }

    public void setExternalLink(Object externalLink) {
        this.externalLink = externalLink;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

//            public List<ProductTranslation> getProductTranslations() {
//                return productTranslations;
//            }
//
//            public void setProductTranslations(List<ProductTranslation> productTranslations) {
//                this.productTranslations = productTranslations;
//            }
//
//            public List<Tax> getTaxes() {
//                return taxes;
//            }
//
//            public void setTaxes(List<Tax> taxes) {
//                this.taxes = taxes;
//            }

    public class ProductTranslation {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("product_id")
        @Expose
        private Integer productId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("unit")
        @Expose
        private String unit;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("lang")
        @Expose
        private String lang;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLang() {
            return lang;
        }

        public void setLang(String lang) {
            this.lang = lang;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public class Tax {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("product_id")
        @Expose
        private Integer productId;
        @SerializedName("tax_id")
        @Expose
        private Integer taxId;
        @SerializedName("tax")
        @Expose
        private Integer tax;
        @SerializedName("tax_type")
        @Expose
        private String taxType;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

        public Integer getTaxId() {
            return taxId;
        }

        public void setTaxId(Integer taxId) {
            this.taxId = taxId;
        }

        public Integer getTax() {
            return tax;
        }

        public void setTax(Integer tax) {
            this.tax = tax;
        }

        public String getTaxType() {
            return taxType;
        }

        public void setTaxType(String taxType) {
            this.taxType = taxType;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

    public class ThumbnailImage {
        public int id;
        public String file_original_name;
        public String file_name;
        public int user_id;
        public int file_size;
        public String extension;
        public String type;
        public Date created_at;
        public Date updated_at;
        public Object deleted_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFile_original_name() {
            return file_original_name;
        }

        public void setFile_original_name(String file_original_name) {
            this.file_original_name = file_original_name;
        }

        public String getFile_name() {
            return file_name;
        }

        public void setFile_name(String file_name) {
            this.file_name = file_name;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getFile_size() {
            return file_size;
        }

        public void setFile_size(int file_size) {
            this.file_size = file_size;
        }

        public String getExtension() {
            return extension;
        }

        public void setExtension(String extension) {
            this.extension = extension;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Date getCreated_at() {
            return created_at;
        }

        public void setCreated_at(Date created_at) {
            this.created_at = created_at;
        }

        public Date getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(Date updated_at) {
            this.updated_at = updated_at;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }
    }

    public List<ThumbnailImage> getThumbnail_images() {
        return thumbnail_images;
    }

    public void setThumbnail_images(List<ThumbnailImage> thumbnail_images) {
        this.thumbnail_images = thumbnail_images;
    }

}
