package com.alifmart.models.categories;

import com.alifmart.models.ProductsPojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Category {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("parent_id")
    @Expose
    private Integer parentId;
    @SerializedName("level")
    @Expose
    private Integer level;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("order_level")
    @Expose
    private Integer orderLevel;
    @SerializedName("commision_rate")
    @Expose
    private Integer commisionRate;
    @SerializedName("banner")
    @Expose
    private String banner;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("featured")
    @Expose
    private Integer featured;
    @SerializedName("top")
    @Expose
    private Integer top;
    @SerializedName("digital")
    @Expose
    private Integer digital;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("meta_title")
    @Expose
    private String metaTitle;
    @SerializedName("meta_description")
    @Expose
    private String metaDescription;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("products_count")
    @Expose
    private Integer productsCount;
    @SerializedName("categories_count")
    @Expose
    private Integer categoriesCount;
    @SerializedName("products")
    @Expose
    private List<Product__1> products = null;
    @SerializedName("categories")
    @Expose
    private List<Category__1> categories = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrderLevel() {
        return orderLevel;
    }

    public void setOrderLevel(Integer orderLevel) {
        this.orderLevel = orderLevel;
    }

    public Integer getCommisionRate() {
        return commisionRate;
    }

    public void setCommisionRate(Integer commisionRate) {
        this.commisionRate = commisionRate;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getFeatured() {
        return featured;
    }

    public void setFeatured(Integer featured) {
        this.featured = featured;
    }

    public Integer getTop() {
        return top;
    }

    public void setTop(Integer top) {
        this.top = top;
    }

    public Integer getDigital() {
        return digital;
    }

    public void setDigital(Integer digital) {
        this.digital = digital;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getMetaTitle() {
        return metaTitle;
    }

    public void setMetaTitle(String metaTitle) {
        this.metaTitle = metaTitle;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getProductsCount() {
        return productsCount;
    }

    public void setProductsCount(Integer productsCount) {
        this.productsCount = productsCount;
    }

    public Integer getCategoriesCount() {
        return categoriesCount;
    }

    public void setCategoriesCount(Integer categoriesCount) {
        this.categoriesCount = categoriesCount;
    }

    public List<Product__1> getProducts() {
        return products;
    }

    public void setProducts(List<Product__1> products) {
        this.products = products;
    }

    public List<Category__1> getCategories() {
        return categories;
    }

    public void setCategories(List<Category__1> categories) {
        this.categories = categories;
    }

    public class Product__1 {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("added_by")
        @Expose
        private String addedBy;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("category_id")
        @Expose
        private Integer categoryId;
        @SerializedName("brand_id")
        @Expose
        private Integer brandId;
        @SerializedName("photos")
        @Expose
        private Object photos;
        @SerializedName("thumbnail_img")
        @Expose
        private Object thumbnailImg;
        @SerializedName("video_provider")
        @Expose
        private String videoProvider;
        @SerializedName("video_link")
        @Expose
        private String videoLink;
        @SerializedName("tags")
        @Expose
        private String tags;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("unit_price")
        @Expose
        private Integer unitPrice;
        @SerializedName("purchase_price")
        @Expose
        private Object purchasePrice;
        @SerializedName("variant_product")
        @Expose
        private Integer variantProduct;
        @SerializedName("attributes")
        @Expose
        private String attributes;
        @SerializedName("choice_options")
        @Expose
        private String choiceOptions;
        @SerializedName("colors")
        @Expose
        private String colors;
        @SerializedName("variations")
        @Expose
        private Object variations;
        @SerializedName("todays_deal")
        @Expose
        private Integer todaysDeal;
        @SerializedName("published")
        @Expose
        private Integer published;
        @SerializedName("approved")
        @Expose
        private Integer approved;
        @SerializedName("stock_visibility_state")
        @Expose
        private String stockVisibilityState;
        @SerializedName("cash_on_delivery")
        @Expose
        private Integer cashOnDelivery;
        @SerializedName("featured")
        @Expose
        private Integer featured;
        @SerializedName("seller_featured")
        @Expose
        private Integer sellerFeatured;
        @SerializedName("current_stock")
        @Expose
        private Integer currentStock;
        @SerializedName("unit")
        @Expose
        private String unit;
        @SerializedName("min_qty")
        @Expose
        private Integer minQty;
        @SerializedName("low_stock_quantity")
        @Expose
        private Integer lowStockQuantity;
        @SerializedName("discount")
        @Expose
        private Integer discount;
        @SerializedName("discount_type")
        @Expose
        private String discountType;
        @SerializedName("discount_start_date")
        @Expose
        private Object discountStartDate;
        @SerializedName("discount_end_date")
        @Expose
        private Object discountEndDate;
        @SerializedName("tax")
        @Expose
        private Object tax;
        @SerializedName("tax_type")
        @Expose
        private Object taxType;
        @SerializedName("shipping_type")
        @Expose
        private String shippingType;
        @SerializedName("shipping_cost")
        @Expose
        private String shippingCost;
        @SerializedName("is_quantity_multiplied")
        @Expose
        private Integer isQuantityMultiplied;
        @SerializedName("est_shipping_days")
        @Expose
        private Object estShippingDays;
        @SerializedName("num_of_sale")
        @Expose
        private Integer numOfSale;
        @SerializedName("meta_title")
        @Expose
        private String metaTitle;
        @SerializedName("meta_description")
        @Expose
        private String metaDescription;
        @SerializedName("meta_img")
        @Expose
        private Object metaImg;
        @SerializedName("pdf")
        @Expose
        private Object pdf;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("barcode")
        @Expose
        private Object barcode;
        @SerializedName("digital")
        @Expose
        private Integer digital;
        @SerializedName("auction_product")
        @Expose
        private Integer auctionProduct;
        @SerializedName("file_name")
        @Expose
        private Object fileName;
        @SerializedName("file_path")
        @Expose
        private Object filePath;
        @SerializedName("external_link")
        @Expose
        private Object externalLink;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("product_translations")
        @Expose
        private List<Product__1.ProductTranslation__1> productTranslations = null;
        @SerializedName("taxes")
        @Expose
        private List<Product__1.Tax__1> taxes = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddedBy() {
            return addedBy;
        }

        public void setAddedBy(String addedBy) {
            this.addedBy = addedBy;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public Integer getBrandId() {
            return brandId;
        }

        public void setBrandId(Integer brandId) {
            this.brandId = brandId;
        }

        public Object getPhotos() {
            return photos;
        }

        public void setPhotos(Object photos) {
            this.photos = photos;
        }

        public Object getThumbnailImg() {
            return thumbnailImg;
        }

        public void setThumbnailImg(Object thumbnailImg) {
            this.thumbnailImg = thumbnailImg;
        }

        public String getVideoProvider() {
            return videoProvider;
        }

        public void setVideoProvider(String videoProvider) {
            this.videoProvider = videoProvider;
        }

        public String getVideoLink() {
            return videoLink;
        }

        public void setVideoLink(String videoLink) {
            this.videoLink = videoLink;
        }

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(Integer unitPrice) {
            this.unitPrice = unitPrice;
        }

        public Object getPurchasePrice() {
            return purchasePrice;
        }

        public void setPurchasePrice(Object purchasePrice) {
            this.purchasePrice = purchasePrice;
        }

        public Integer getVariantProduct() {
            return variantProduct;
        }

        public void setVariantProduct(Integer variantProduct) {
            this.variantProduct = variantProduct;
        }

        public String getAttributes() {
            return attributes;
        }

        public void setAttributes(String attributes) {
            this.attributes = attributes;
        }

        public String getChoiceOptions() {
            return choiceOptions;
        }

        public void setChoiceOptions(String choiceOptions) {
            this.choiceOptions = choiceOptions;
        }

        public String getColors() {
            return colors;
        }

        public void setColors(String colors) {
            this.colors = colors;
        }

        public Object getVariations() {
            return variations;
        }

        public void setVariations(Object variations) {
            this.variations = variations;
        }

        public Integer getTodaysDeal() {
            return todaysDeal;
        }

        public void setTodaysDeal(Integer todaysDeal) {
            this.todaysDeal = todaysDeal;
        }

        public Integer getPublished() {
            return published;
        }

        public void setPublished(Integer published) {
            this.published = published;
        }

        public Integer getApproved() {
            return approved;
        }

        public void setApproved(Integer approved) {
            this.approved = approved;
        }

        public String getStockVisibilityState() {
            return stockVisibilityState;
        }

        public void setStockVisibilityState(String stockVisibilityState) {
            this.stockVisibilityState = stockVisibilityState;
        }

        public Integer getCashOnDelivery() {
            return cashOnDelivery;
        }

        public void setCashOnDelivery(Integer cashOnDelivery) {
            this.cashOnDelivery = cashOnDelivery;
        }

        public Integer getFeatured() {
            return featured;
        }

        public void setFeatured(Integer featured) {
            this.featured = featured;
        }

        public Integer getSellerFeatured() {
            return sellerFeatured;
        }

        public void setSellerFeatured(Integer sellerFeatured) {
            this.sellerFeatured = sellerFeatured;
        }

        public Integer getCurrentStock() {
            return currentStock;
        }

        public void setCurrentStock(Integer currentStock) {
            this.currentStock = currentStock;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public Integer getMinQty() {
            return minQty;
        }

        public void setMinQty(Integer minQty) {
            this.minQty = minQty;
        }

        public Integer getLowStockQuantity() {
            return lowStockQuantity;
        }

        public void setLowStockQuantity(Integer lowStockQuantity) {
            this.lowStockQuantity = lowStockQuantity;
        }

        public Integer getDiscount() {
            return discount;
        }

        public void setDiscount(Integer discount) {
            this.discount = discount;
        }

        public String getDiscountType() {
            return discountType;
        }

        public void setDiscountType(String discountType) {
            this.discountType = discountType;
        }

        public Object getDiscountStartDate() {
            return discountStartDate;
        }

        public void setDiscountStartDate(Object discountStartDate) {
            this.discountStartDate = discountStartDate;
        }

        public Object getDiscountEndDate() {
            return discountEndDate;
        }

        public void setDiscountEndDate(Object discountEndDate) {
            this.discountEndDate = discountEndDate;
        }

        public Object getTax() {
            return tax;
        }

        public void setTax(Object tax) {
            this.tax = tax;
        }

        public Object getTaxType() {
            return taxType;
        }

        public void setTaxType(Object taxType) {
            this.taxType = taxType;
        }

        public String getShippingType() {
            return shippingType;
        }

        public void setShippingType(String shippingType) {
            this.shippingType = shippingType;
        }

        public String getShippingCost() {
            return shippingCost;
        }

        public void setShippingCost(String shippingCost) {
            this.shippingCost = shippingCost;
        }

        public Integer getIsQuantityMultiplied() {
            return isQuantityMultiplied;
        }

        public void setIsQuantityMultiplied(Integer isQuantityMultiplied) {
            this.isQuantityMultiplied = isQuantityMultiplied;
        }

        public Object getEstShippingDays() {
            return estShippingDays;
        }

        public void setEstShippingDays(Object estShippingDays) {
            this.estShippingDays = estShippingDays;
        }

        public Integer getNumOfSale() {
            return numOfSale;
        }

        public void setNumOfSale(Integer numOfSale) {
            this.numOfSale = numOfSale;
        }

        public String getMetaTitle() {
            return metaTitle;
        }

        public void setMetaTitle(String metaTitle) {
            this.metaTitle = metaTitle;
        }

        public String getMetaDescription() {
            return metaDescription;
        }

        public void setMetaDescription(String metaDescription) {
            this.metaDescription = metaDescription;
        }

        public Object getMetaImg() {
            return metaImg;
        }

        public void setMetaImg(Object metaImg) {
            this.metaImg = metaImg;
        }

        public Object getPdf() {
            return pdf;
        }

        public void setPdf(Object pdf) {
            this.pdf = pdf;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public Object getBarcode() {
            return barcode;
        }

        public void setBarcode(Object barcode) {
            this.barcode = barcode;
        }

        public Integer getDigital() {
            return digital;
        }

        public void setDigital(Integer digital) {
            this.digital = digital;
        }

        public Integer getAuctionProduct() {
            return auctionProduct;
        }

        public void setAuctionProduct(Integer auctionProduct) {
            this.auctionProduct = auctionProduct;
        }

        public Object getFileName() {
            return fileName;
        }

        public void setFileName(Object fileName) {
            this.fileName = fileName;
        }

        public Object getFilePath() {
            return filePath;
        }

        public void setFilePath(Object filePath) {
            this.filePath = filePath;
        }

        public Object getExternalLink() {
            return externalLink;
        }

        public void setExternalLink(Object externalLink) {
            this.externalLink = externalLink;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<Product__1.ProductTranslation__1> getProductTranslations() {
            return productTranslations;
        }

        public void setProductTranslations(List<Product__1.ProductTranslation__1> productTranslations) {
            this.productTranslations = productTranslations;
        }

        public List<Product__1.Tax__1> getTaxes() {
            return taxes;
        }

        public void setTaxes(List<Product__1.Tax__1> taxes) {
            this.taxes = taxes;
        }

        public class ProductTranslation__1 {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("product_id")
            @Expose
            private Integer productId;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("unit")
            @Expose
            private String unit;
            @SerializedName("description")
            @Expose
            private String description;
            @SerializedName("lang")
            @Expose
            private String lang;
            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getProductId() {
                return productId;
            }

            public void setProductId(Integer productId) {
                this.productId = productId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getUnit() {
                return unit;
            }

            public void setUnit(String unit) {
                this.unit = unit;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getLang() {
                return lang;
            }

            public void setLang(String lang) {
                this.lang = lang;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

        }
        public class Tax__1 {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("product_id")
            @Expose
            private Integer productId;
            @SerializedName("tax_id")
            @Expose
            private Integer taxId;
            @SerializedName("tax")
            @Expose
            private Integer tax;
            @SerializedName("tax_type")
            @Expose
            private String taxType;
            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getProductId() {
                return productId;
            }

            public void setProductId(Integer productId) {
                this.productId = productId;
            }

            public Integer getTaxId() {
                return taxId;
            }

            public void setTaxId(Integer taxId) {
                this.taxId = taxId;
            }

            public Integer getTax() {
                return tax;
            }

            public void setTax(Integer tax) {
                this.tax = tax;
            }

            public String getTaxType() {
                return taxType;
            }

            public void setTaxType(String taxType) {
                this.taxType = taxType;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

        }

    }

    public class Category__1 {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("parent_id")
        @Expose
        private Integer parentId;
        @SerializedName("level")
        @Expose
        private Integer level;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("order_level")
        @Expose
        private Integer orderLevel;
        @SerializedName("commision_rate")
        @Expose
        private Integer commisionRate;
        @SerializedName("banner")
        @Expose
        private String banner;
        @SerializedName("icon")
        @Expose
        private String icon;
        @SerializedName("featured")
        @Expose
        private Integer featured;
        @SerializedName("top")
        @Expose
        private Integer top;
        @SerializedName("digital")
        @Expose
        private Integer digital;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("meta_title")
        @Expose
        private String metaTitle;
        @SerializedName("meta_description")
        @Expose
        private String metaDescription;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("products")
        @Expose
        private List<Object> products = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getParentId() {
            return parentId;
        }

        public void setParentId(Integer parentId) {
            this.parentId = parentId;
        }

        public Integer getLevel() {
            return level;
        }

        public void setLevel(Integer level) {
            this.level = level;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getOrderLevel() {
            return orderLevel;
        }

        public void setOrderLevel(Integer orderLevel) {
            this.orderLevel = orderLevel;
        }

        public Integer getCommisionRate() {
            return commisionRate;
        }

        public void setCommisionRate(Integer commisionRate) {
            this.commisionRate = commisionRate;
        }

        public String getBanner() {
            return banner;
        }

        public void setBanner(String banner) {
            this.banner = banner;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public Integer getFeatured() {
            return featured;
        }

        public void setFeatured(Integer featured) {
            this.featured = featured;
        }

        public Integer getTop() {
            return top;
        }

        public void setTop(Integer top) {
            this.top = top;
        }

        public Integer getDigital() {
            return digital;
        }

        public void setDigital(Integer digital) {
            this.digital = digital;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getMetaTitle() {
            return metaTitle;
        }

        public void setMetaTitle(String metaTitle) {
            this.metaTitle = metaTitle;
        }

        public String getMetaDescription() {
            return metaDescription;
        }

        public void setMetaDescription(String metaDescription) {
            this.metaDescription = metaDescription;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<Object> getProducts() {
            return products;
        }

        public void setProducts(List<Object> products) {
            this.products = products;
        }

    }

}

