package com.alifmart.models.loginregistration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("token")
    @Expose
    private String token;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public class User {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("referred_by")
        @Expose
        private Object referredBy;
        @SerializedName("provider_id")
        @Expose
        private Object providerId;
        @SerializedName("user_type")
        @Expose
        private String userType;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("email_verified_at")
        @Expose
        private Object emailVerifiedAt;
        @SerializedName("verification_code")
        @Expose
        private Object verificationCode;
        @SerializedName("new_email_verificiation_code")
        @Expose
        private Object newEmailVerificiationCode;
        @SerializedName("device_token")
        @Expose
        private Object deviceToken;
        @SerializedName("avatar")
        @Expose
        private Object avatar;
        @SerializedName("avatar_original")
        @Expose
        private Object avatarOriginal;
        @SerializedName("address")
        @Expose
        private Object address;
        @SerializedName("country")
        @Expose
        private Object country;
        @SerializedName("city")
        @Expose
        private Object city;
        @SerializedName("postal_code")
        @Expose
        private Object postalCode;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("balance")
        @Expose
        private String balance;
        @SerializedName("banned")
        @Expose
        private String banned;
        @SerializedName("referral_code")
        @Expose
        private Object referralCode;
        @SerializedName("customer_package_id")
        @Expose
        private Object customerPackageId;
        @SerializedName("remaining_uploads")
        @Expose
        private String remainingUploads;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("nid")
        @Expose
        private Object nid;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Object getReferredBy() {
            return referredBy;
        }

        public void setReferredBy(Object referredBy) {
            this.referredBy = referredBy;
        }

        public Object getProviderId() {
            return providerId;
        }

        public void setProviderId(Object providerId) {
            this.providerId = providerId;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getEmailVerifiedAt() {
            return emailVerifiedAt;
        }

        public void setEmailVerifiedAt(Object emailVerifiedAt) {
            this.emailVerifiedAt = emailVerifiedAt;
        }

        public Object getVerificationCode() {
            return verificationCode;
        }

        public void setVerificationCode(Object verificationCode) {
            this.verificationCode = verificationCode;
        }

        public Object getNewEmailVerificiationCode() {
            return newEmailVerificiationCode;
        }

        public void setNewEmailVerificiationCode(Object newEmailVerificiationCode) {
            this.newEmailVerificiationCode = newEmailVerificiationCode;
        }

        public Object getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(Object deviceToken) {
            this.deviceToken = deviceToken;
        }

        public Object getAvatar() {
            return avatar;
        }

        public void setAvatar(Object avatar) {
            this.avatar = avatar;
        }

        public Object getAvatarOriginal() {
            return avatarOriginal;
        }

        public void setAvatarOriginal(Object avatarOriginal) {
            this.avatarOriginal = avatarOriginal;
        }

        public Object getAddress() {
            return address;
        }

        public void setAddress(Object address) {
            this.address = address;
        }

        public Object getCountry() {
            return country;
        }

        public void setCountry(Object country) {
            this.country = country;
        }

        public Object getCity() {
            return city;
        }

        public void setCity(Object city) {
            this.city = city;
        }

        public Object getPostalCode() {
            return postalCode;
        }

        public void setPostalCode(Object postalCode) {
            this.postalCode = postalCode;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getBalance() {
            return balance;
        }

        public void setBalance(String balance) {
            this.balance = balance;
        }

        public String getBanned() {
            return banned;
        }

        public void setBanned(String banned) {
            this.banned = banned;
        }

        public Object getReferralCode() {
            return referralCode;
        }

        public void setReferralCode(Object referralCode) {
            this.referralCode = referralCode;
        }

        public Object getCustomerPackageId() {
            return customerPackageId;
        }

        public void setCustomerPackageId(Object customerPackageId) {
            this.customerPackageId = customerPackageId;
        }

        public String getRemainingUploads() {
            return remainingUploads;
        }

        public void setRemainingUploads(String remainingUploads) {
            this.remainingUploads = remainingUploads;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getNid() {
            return nid;
        }

        public void setNid(Object nid) {
            this.nid = nid;
        }

    }

}
