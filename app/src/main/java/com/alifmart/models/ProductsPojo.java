package com.alifmart.models;

import com.alifmart.models.brands.Brand;
import com.alifmart.models.categories.Category;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.util.List;

public class ProductsPojo {

    @SerializedName("brands")
    @Expose
    private List<Brand> brands = null;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

}
