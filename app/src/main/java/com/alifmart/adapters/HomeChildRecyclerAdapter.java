package com.alifmart.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alifmart.R;
import com.alifmart.models.brands.Product;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class HomeChildRecyclerAdapter extends RecyclerView.Adapter<HomeChildRecyclerAdapter.HomeChildViewHolder>{

    private Context context;
    private List<Product> allProductsForSingleBrandList = new ArrayList<>();

    public HomeChildRecyclerAdapter(Context context, List<Product> allProductsForSingleBrandList) {
        this.context = context;
        this.allProductsForSingleBrandList = allProductsForSingleBrandList;
    }

    @NonNull
    @Override
    public HomeChildViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_child_rv, parent, false);
        return new HomeChildViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeChildViewHolder holder, int position) {

        if (allProductsForSingleBrandList != null){

            holder.productNameTV.setText(allProductsForSingleBrandList.get(position).getName());

            String productImg = "https://shop.alifmartbd.com/" + allProductsForSingleBrandList.get(position).getThumbnail_images().get(0).getFile_name();
            Glide.with(context)
                    .load(productImg)
                    .fitCenter()
                    .into(holder.productPhotoIV);

            holder.productNewPriceTV.setText(String.valueOf(allProductsForSingleBrandList.get(position).getUnitPrice()));
            //holder.productDiscountTV.setText(allProductsForSingleBrandList.get(position).getDiscount());

        }

    }

    @Override
    public int getItemCount() {
        return allProductsForSingleBrandList.size();
    }

    static class HomeChildViewHolder extends RecyclerView.ViewHolder{

        private ImageView productPhotoIV;
        private TextView productNameTV, productOldPriceTV, productNewPriceTV, productDiscountTV;

        public HomeChildViewHolder(@NonNull View itemView) {
            super(itemView);

            productPhotoIV = itemView.findViewById(R.id.home_child_product_iv);
            productNameTV = itemView.findViewById(R.id.home_child_product_name_tv);
            productOldPriceTV = itemView.findViewById(R.id.home_child_product_old_price_tv);
            productNewPriceTV = itemView.findViewById(R.id.home_child_product_new_price_tv);
            productDiscountTV = itemView.findViewById(R.id.home_child_product_discount_tv);
        }
    }
}
