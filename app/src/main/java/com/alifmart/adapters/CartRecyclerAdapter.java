package com.alifmart.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alifmart.R;
import com.alifmart.cache.entities.CartProductDetails;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class CartRecyclerAdapter extends RecyclerView.Adapter<CartRecyclerAdapter.CartViewHolder> {

    private Context context;
    private List<CartProductDetails> cartProductDetailsList;

    public CartRecyclerAdapter(Context context, List<CartProductDetails> cartProductDetailsList) {
        this.context = context;
        this.cartProductDetailsList = cartProductDetailsList;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_cart_recycler_product, parent, false);
        return new CartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {

        if (!cartProductDetailsList.isEmpty()) {
            holder.cartProductName.setText(cartProductDetailsList.get(position).getProductName());
            holder.cartProductPrice.setText(String.valueOf(cartProductDetailsList.get(position).getProductPrice()));

            String imageURL = "https://shop.alifmartbd.com/" + cartProductDetailsList.get(position).getProductThumbnail();
            Glide.with(context)
                    .load(imageURL)
                    .fitCenter()
                    .into(holder.cartThumbnailIV);

            holder.cartDeleteIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return cartProductDetailsList.size();
    }

    static class CartViewHolder extends RecyclerView.ViewHolder {

        ImageView cartThumbnailIV, cartDeleteIV;
        TextView cartProductName, cartProductPrice;

        public CartViewHolder(@NonNull View itemView) {
            super(itemView);

            cartThumbnailIV = itemView.findViewById(R.id.cart_product_thumnail);
            cartDeleteIV = itemView.findViewById(R.id.cart_product_delete);
            cartProductName = itemView.findViewById(R.id.cart_product_name);
            cartProductPrice = itemView.findViewById(R.id.cart_product_price);
        }
    }
}
