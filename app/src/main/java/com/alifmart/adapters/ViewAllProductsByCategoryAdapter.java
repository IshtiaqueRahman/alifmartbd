package com.alifmart.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alifmart.R;
import com.alifmart.models.categoryproducts.Product;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class ViewAllProductsByCategoryAdapter extends RecyclerView.Adapter<ViewAllProductsByCategoryAdapter.ByCategoryViewHolder>{

    private Context context;
    private List<Product> allProducts = new ArrayList<>();

    public ViewAllProductsByCategoryAdapter(Context context, List<Product> allProducts) {
        this.context = context;
        this.allProducts = allProducts;
    }

    @NonNull
    @Override
    public ByCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_child_rv, parent, false);
        return new ByCategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ByCategoryViewHolder holder, int position) {

        if (allProducts != null){

            holder.productNameTV.setText(allProducts.get(position).getName());

            String imageURl = allProducts.get(position).getThumbnailImages().get(0).getFileName();
            if (!imageURl.isEmpty()){
                String productImg = "https://shop.alifmartbd.com/" + imageURl;
                Glide.with(context)
                        .load(productImg)
                        .fitCenter()
                        .into(holder.productPhotoIV);
            }


            holder.productNewPriceTV.setText(String.valueOf(allProducts.get(position).getUnitPrice()));

        }

    }

    @Override
    public int getItemCount() {
        return allProducts.size();
    }

    static class ByCategoryViewHolder extends RecyclerView.ViewHolder {

        private ImageView productPhotoIV;
        private TextView productNameTV, productOldPriceTV, productNewPriceTV, productDiscountTV;

        public ByCategoryViewHolder(@NonNull View itemView) {
            super(itemView);

            productPhotoIV = itemView.findViewById(R.id.home_child_product_iv);
            productNameTV = itemView.findViewById(R.id.home_child_product_name_tv);
            productOldPriceTV = itemView.findViewById(R.id.home_child_product_old_price_tv);
            productNewPriceTV = itemView.findViewById(R.id.home_child_product_new_price_tv);
            productDiscountTV = itemView.findViewById(R.id.home_child_product_discount_tv);
        }
    }
}
