package com.alifmart.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alifmart.R;
import com.alifmart.models.brands.Brand;
import com.alifmart.models.brands.Product;
import com.alifmart.ui.home.HomeFragmentDirections;
import com.alifmart.utils.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

public class HomeParentRecyclerAdapter extends RecyclerView.Adapter<HomeParentRecyclerAdapter.HomeParentViewHolder>{

    private Context context;
    private List<Brand> allProductListByBrand = new ArrayList<>();
    private RecyclerView.RecycledViewPool recycledViewPool = new RecyclerView.RecycledViewPool();

    public HomeParentRecyclerAdapter(Context context, List<Brand> allProductListByBrand) {
        this.context = context;
        this.allProductListByBrand = allProductListByBrand;
    }

    @NonNull
    @Override
    public HomeParentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_parent_rv, parent, false);
        return new HomeParentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeParentViewHolder holder, int position) {

        if (allProductListByBrand != null){

            holder.brandNameTV.setText(allProductListByBrand.get(position).getName());

            //view all products
            int brandId = allProductListByBrand.get(position).getId();
            holder.brandViewAllTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavDirections directions = HomeFragmentDirections.actionNavigationHomeToNavigationViewAllBrandProducts(brandId);
                    Navigation.findNavController(v).navigate(directions);
                }
            });

            //child recycler
            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            List<Product> productList = allProductListByBrand.get(position).getProducts();
            HomeChildRecyclerAdapter homeChildRecyclerAdapter = new HomeChildRecyclerAdapter(context, allProductListByBrand.get(position).getProducts());
            holder.allBrandProductRV.setLayoutManager(layoutManager);
            holder.allBrandProductRV.setRecycledViewPool(recycledViewPool);
            holder.allBrandProductRV.setAdapter(homeChildRecyclerAdapter);


            // child view on click listner
            holder.allBrandProductRV.addOnItemTouchListener(new RecyclerTouchListener(context, holder.allBrandProductRV, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    int productId = productList.get(position).getId();

                    NavDirections directions = HomeFragmentDirections.actionNavigationHomeToNavigationProductDetails(productId);
                    Navigation.findNavController(view).navigate(directions);
                }
                @Override
                public void onLongClick(View view, int position) {

                }
            }));

        }

    }

    @Override
    public int getItemCount() {
        return allProductListByBrand.size();
    }

    static class HomeParentViewHolder extends RecyclerView.ViewHolder{

        private TextView brandNameTV;
        private TextView brandViewAllTV;
        private RecyclerView allBrandProductRV;

        public HomeParentViewHolder(@NonNull View itemView) {
            super(itemView);

            brandNameTV = itemView.findViewById(R.id.home_parent_brand_name_tv);
            brandViewAllTV = itemView.findViewById(R.id.home_parent_all_tv);
            allBrandProductRV = itemView.findViewById(R.id.home_parent_child_rv);
        }
    }

}
