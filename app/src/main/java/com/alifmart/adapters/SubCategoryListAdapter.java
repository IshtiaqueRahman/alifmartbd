package com.alifmart.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alifmart.R;
import com.alifmart.models.listcategory.Category;

import java.util.ArrayList;
import java.util.List;

public class SubCategoryListAdapter extends RecyclerView.Adapter<SubCategoryListAdapter.SubCategoryViewHolder> {

    private Context context;
    private List<Category> allSubCategory = new ArrayList<>();

    public SubCategoryListAdapter(Context context, List<Category> allSubCategory) {
        this.context = context;
        this.allSubCategory = allSubCategory;
    }


    @NonNull
    @Override
    public SubCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_categories_rv, parent, false);
        return new SubCategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubCategoryViewHolder holder, int position) {

        if (!allSubCategory.isEmpty()) {
            holder.categoryNameTV.setText(allSubCategory.get(position).getName());
        }

    }

    @Override
    public int getItemCount() {
        return allSubCategory.size();
    }

    static class SubCategoryViewHolder extends RecyclerView.ViewHolder {

        private ImageView categoryIV;
        private TextView categoryNameTV;

        public SubCategoryViewHolder(@NonNull View itemView) {
            super(itemView);

            categoryIV = itemView.findViewById(R.id.row_category_iv);
            categoryNameTV = itemView.findViewById(R.id.row_category_name_tv);
        }
    }
}
